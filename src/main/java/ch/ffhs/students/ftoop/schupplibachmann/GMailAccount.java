package ch.ffhs.students.ftoop.schupplibachmann;

import javax.mail.*;
import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.Properties;

/**
 * Created by root on 29.05.17.
 */
public class GMailAccount {
    // The IP address of the POP3 server
    String host;
    // Username and password
    String user;
    String password;
    // mail store of gmail server account
    Store mailStore;
    // the actual session to the server
    Session session;
    // properties
    Properties props;
    // folders of the account
    LinkedList<Folder> mailFolders;
    LinkedList<Message> mailsReceived = new LinkedList<Message>();

    public GMailAccount(String username, String password)
    {
        this.host = "pop.gmail.com" ;
        this.user = username;
        this.password = password;

        // Get system properties
        props = new Properties();

        // Request POP3S
        props.put("mail.store.protocol", "pop3s");
        props.put("mail.pop3s.auth", "true");
        props.put("mail.pop3s.port", "995");
        props.put("mail.host", "pop.gmail.com");
    }

    public void ReceiveMails() throws Exception {
        new SwingWorker()
        {
            @Override
            protected Integer doInBackground() throws Exception {
                // Get the default Session object
                Session session = Session.getDefaultInstance(props);
                // Get a store for the POP3S protocol
                Store store = session.getStore();

                // Connect to the current host using the specified username and password
                store.connect(host, user, password);

                // Create a Folder object corresponding to the given name
                Folder folder = store.getFolder("inbox");

                // Open the Folder
                folder.open(Folder.READ_WRITE );

                // Get the messages from the server
                Message[] newMails = folder.getMessages();
                mailsReceived.clear();
                for (Message msg: newMails) {
                    mailsReceived.add(msg);
                    msg.setFlag(Flags.Flag.DELETED, true);
                }
                return 1;
            }

            @Override
            public void done() {
                fireReceivedNewMails(new NewMailsReceivedEvent(this, mailsReceived));
            }
        }.execute();
    }

    private EventListenerList listenerList = new EventListenerList();

    void addNewMailsReceivedListener(NewMailsReceivedListener l) {
        listenerList.add(NewMailsReceivedListener.class, l);
    }

    void removeNewMailsReceivedListener(NewMailsReceivedListener l) {
        listenerList.remove(NewMailsReceivedListener.class, l);
    }

    // Roughly analogous to .net OnEvent protected virtual method pattern -
    // call this method to raise the event
    protected void fireReceivedNewMails(NewMailsReceivedEvent e) {
        NewMailsReceivedListener[] ls = listenerList.getListeners(NewMailsReceivedListener.class);
        for (NewMailsReceivedListener l : ls) {
            l.NewMailsReceived(e);
        }
    }
}

// Roughly analogous to .NET EventArgs
class NewMailsReceivedEvent extends EventObject {
    private LinkedList<Message> _messages;

    public NewMailsReceivedEvent( Object source, LinkedList<Message> messages ) {
        super( source );
        _messages = messages;
    }
    public LinkedList<Message> Messages() {
        return _messages;
    }
}

// Roughly analogous to .NET delegate
interface NewMailsReceivedListener extends EventListener {
    void NewMailsReceived(NewMailsReceivedEvent e);
}
