package ch.ffhs.students.ftoop.schupplibachmann;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.Vector;

/**
 * Created by root on 29.05.17.
 */
public class MailClientWindow {

    private JPanel MainPanel;
    private JTree MailFolders;
    private JList list1;
    private JTextArea textArea1;
    private JButton btnNewMail;
    private JButton btnDeleteMail;
    private JButton btnReplyMail;
    private JButton btnForwardMail;
    private JButton btnGetMails;
    private JLabel lblError;
    private GMailAccount gmailAcc;

    private DefaultListModel<Message> listmodel = new DefaultListModel<Message>();

    public MailClientWindow() {
        gmailAcc = new GMailAccount("whateverittakes70@gmail.com", "ffhs2017");
        list1.setModel(listmodel);
        list1.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (renderer instanceof JLabel && value instanceof Message) {
                    // Here value will be of the Type 'CD'
                    try {
                        ((JLabel) renderer).setText(((Message) value).getSubject());
                    } catch (MessagingException e) {
                        ((JLabel) renderer).setText("<<FEHLER IN BETREFF>>");
                    }
                }
                return renderer;
            }
        });
        gmailAcc.addNewMailsReceivedListener(
            e -> {
                listmodel.clear();
                for(Message msg: e.Messages())
                {
                    listmodel.addElement(msg);
                }
                list1.invalidate();
            }
        );

        btnGetMails.addActionListener(actionEvent -> {
            try {
                gmailAcc.ReceiveMails();
            } catch (MessagingException e) {
                lblError.setVisible(true);
                lblError.setText("Es ist ein Fehler beim Empfangen der Mails aufgetretten");
            } catch (Exception e)
            {
                lblError.setVisible(true);
                lblError.setText("Grober Fehler aufgetretten");
            }
        });
        btnNewMail.addActionListener(actionEvent -> {
            //TODO
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Mail Client by SNAFU - the only real one");
        //frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        frame.setContentPane(new MailClientWindow().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}